from django.db import migrations, models


class Book(models.Model):

    name= models.CharField(
        "Название книги",
        max_length=200,
        null=True,
        blank=True)

    ISBN = models.CharField(
        "International Standard Book Number",
        max_length=200,
        null=True,
        blank=True)

    age_limit = models.BooleanField(
        "Вам есть 18 лет?",
        default=True)

    image=models.ImageField(
        "Обложка",
        upload_to="media/img",
        blank=True,
        null=True)

    price=models.DecimalField(
        "Цена",
        max_digits=8,
        decimal_places=2)

    serie = models.ForeignKey(
        "catalog.Serie",
        related_name="books",
        verbose_name="Серия",
        on_delete=models.PROTECT)
# related_name="любое слово" - собрать
#  несколько значений из другой базы данных с определенным атрибутом син 
# "назв базы"."значение записанное в related_name".all
    last_name = models.ManyToManyField(
        "catalog.Authors",
        verbose_name="Авторы")

    format_b = models.ForeignKey(
        "catalog.Format",
        related_name="books",
        verbose_name="Формат книги",
        on_delete=models.PROTECT)

    issue_year=models.IntegerField("Год издания", 
        null=True)

    available=models.BooleanField(
        "Доступно для заказа",
        default=True)
    #Активный ли из ТЗ (Галочка)

    rate=models.FloatField(
        "Рейтинг",
        default=0)

    #created_date=str(models.DateTimeField(
    created_date = models.DateTimeField(
        "Дата создания",
        auto_now=True,
      #  auto_now_add=True,
      #  default="2000-01-01 00:00"
    )
    #created_date

    #date_of_change=str(models.DateTimeField(
    date_of_change = models.DateTimeField(
        "Дата изменения",
        auto_now=True,
     #   auto_now_add=False,
      #  default="2000-01-01 00:00"
    )

    page = models.DecimalField(
        "Количество страниц",
        max_digits=8,
        decimal_places=2)

    book_binding = models.ForeignKey(
        "catalog.binding",
        related_name="books",
        verbose_name="Переплет",
        on_delete=models.PROTECT)

    weight = models.DecimalField(
        "Вес книги",
        max_digits=8,
        decimal_places=0)

    class Meta:
        verbose_name="Книга"
        verbose_name_plural="Книги"
    def __str__(self):
        return self.name
# Create your models here.
