# Generated by Django 2.2 on 2019-05-07 07:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0002_auto_20190427_1341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='media/img', verbose_name='Обложка'),
        ),
    ]
