from django.urls import path, include
from django.contrib import admin
from django.conf.urls import url
from catalog.views import *

from django.conf import settings
from django.conf.urls.static import static


#Нижеперечисленное делать не будем (функциональный подход)

# from django.shortcuts import render
# from refs.models import Serie

# def test_view(request):
#   serie=Serie.objects.last()
#   context={"object":serie}
#   return render(request, "test.html, context")
# В файле HTML в двой ных скобках забиваем {{object}}
# далее прописывем поля

# Заканчиваем то, с чем мы работать не будем 

urlpatterns = [

    path("", include("home.urls")),

    path('admin/', admin.site.urls),
    path("user/", include("user.urls")),
    path("Списки/", include("Списки.urls")),
    path("home/", include("mainApp.urls")),

    path("allreferens/", AllReferenses.as_view(), name="allrefers"),


    path("authors/<int:pk>", AuthorsDetail.as_view(), name="authors-detail"),
    path("authors/<int:pk>/update", AuthorsUpdateView.as_view(), name="authors-update"),
    path("authors/", AuthorsList.as_view(), name="authors-list-view"),
    path("authors/create/", AuthorsCreateView.as_view(), name="authors-create"), 
    path("authors/<int:pk>/delete", AuthorsDeleteView.as_view(), name="authors-delete"),
    
    path("book/<int:pk>", BookDetail.as_view(), name="book-detail"),
    path("book/<int:pk>/update", BookUpdateView.as_view(), name="book-update"),
    path("book/", BookList.as_view(), name="book-list"),
    path("book/create/", BookCreateView.as_view(), name="book-create"),
    path("book/<int:pk>/delete", BookDeleteView.as_view(), name="book-delete"),


    path("serie/<int:pk>", SerieDetail.as_view(), name="serie-detail"),
    path("serie/", SerieList.as_view(), name="serie-list-view"),
    path("catalog/serie/create/", SerieCreateView.as_view(), name="serie-create"),
    path("serie/<int:pk>/update", SerieUpdateView.as_view(), name="serie-update"),
    path("serie/<int:pk>/delete",SerieDeleteView.as_view(), name="serie-delete"),

    path("format/<int:pk>", FormatDetail.as_view(), name="format-detail"),
    path("format/", FormatList.as_view(), name="format-list-view"),
    path("format/create/", FormatCreateView.as_view(), name="format-create"),
    path("format/<int:pk>/update", FormatUpdateView.as_view(), name="format-update"),
    path("format/<int:pk>/delete", FormatDeleteView.as_view(), name="format-list-view"),

    path("binding/<int:pk>", bindingDetail.as_view(), name="binding-detail"),
    path("binding/<int:pk>/update", bindingUpdateView.as_view(), name="binding-update"),
    path("binding/<int:pk>/delete", bindingDeleteView.as_view(), name="binding-delete"),
    path("binding/", bindingList.as_view(), name="binding-list-view"),
    path("binding/create/", bindingCreateView.as_view(), name="binding-create"),

    path("genres/<int:pk>", GenresDetail.as_view(), name="genres-detail"),
    path("genres/<int:pk>/update", GenresUpdate.as_view(), name="genres-update"),
    path("genres/<int:pk>/delete", GenresDelete.as_view(), name="genres-delete"),
    path("genres/", GenresList.as_view(), name="genres-list-view"),
    path("genres/create/", GenresCreateView.as_view(), name="genres-create"),

    path("publisher/<int:pk>", PublisherDetail.as_view()),
    path("publisher/", PublisherList.as_view(), name="publisher-list-view"),
    path("catalog/publisher/create/", PublisherCreateView.as_view()),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
