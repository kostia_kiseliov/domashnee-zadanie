from django.shortcuts import render
from django.views.generic.base import TemplateView
from home.models import Home
from django.views.generic.list import ListView

def index(request):
    return render(request, "home/home_list.html")

class HomeViews(TemplateView):
    model=Home
    template_name="home/home.html"

class HomeList(ListView):
    model=Home


#    def get_queryset(self, kwargs):
#        h = super().get_queryset(**kwargs)
#        n = self.request.GET.get("search", 0)
#        if n==0:
#            return h
#        return h.filter(name=n)

# Create your views here.
