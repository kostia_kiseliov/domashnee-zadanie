from django.db import migrations, models

class Home(models.Model):
    name=models.ForeignKey(
        "catalog.Serie",
        related_name="home",
        on_delete=models.PROTECT
    )
    last_name = models.ManyToManyField(
        "catalog.Authors",
        verbose_name="Авторы")
