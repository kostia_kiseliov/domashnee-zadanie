from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'user'

class BookConfig(AppConfig):
    name = "book"
