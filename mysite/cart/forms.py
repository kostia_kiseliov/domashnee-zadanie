from django import forms
from django.forms import ModelForm
from cart.models import BookCart
class AddProductForm(ModelForm):
    class Meta:
        model = BookCart
        fields = ['cart', 'book', 'quantity']
        widgets = {'cart': forms.HiddenInput, 'book': forms.HiddenInput}
