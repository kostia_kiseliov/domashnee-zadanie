from django.shortcuts import render
from cart.models import BookCart
from django.views.generic.edit import UpdateView
# Create your views here.
class AddProduct(UpdateView):
    model = BookCard
    form_class = AddProductForm
    template_name = 'cart/cart.html'

    def get_object(self, queryset=None):
        cart_id = self.request.session.get('cart_id')
        self.request.session['cart_id'] = cart.pk
        book_pk = self.kwargs.get('pk')
        book = Book.objects.get(pk=book_pk)
        book_in_cart, created = self.model.objects.get_or_create(cart=cart, book=book, defaults={'quantity': 1})
        if not created:
            book_in_cart.quantity += 1
        return book_in_cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['book_id'] = self.kwargs.get('pk')
        return context
