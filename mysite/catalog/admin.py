from . import models
from django.contrib import admin
from catalog.models import Serie, Authors, Genres, Publisher, binding, Format
from book.models import Book


admin.site.register(Serie)
admin.site.register(Authors)
admin.site.register(Genres)
admin.site.register(Publisher)
admin.site.register(Book)
admin.site.register(binding)
admin.site.register(Format)



#ModelAdmine.list
class SerieAdmin(admin.ModelAdmin):
    list_display = [
        "series"
    ]
    list_filter = ( "series")

# Register your models here.

