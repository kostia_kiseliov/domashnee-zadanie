from django.db import migrations, models
from django.urls import reverse_lazy


class Authors(models.Model):

    first_name = models.CharField("Имя", max_length=40)
    last_name = models.CharField("Фамилия", max_length=40)
    middle_name = models.CharField("Отчество", max_length=40)

    class Meta:

        db_table = 'Авторы'
        verbose_name_plural = "Авторы"

    def __str__(self):
        template='{0.last_name} {0.first_name} {0.middle_name}'
        return template.format(self)

    def get_absolute_url(self):
        return reverse_lazy("authors-list-view")

class Serie(models.Model):
    # pk
    series = models.TextField(
        "Серия")
    
    
    class Meta:
        db_table = 'Серия'
        verbose_name_plural = "Серии"
        
    def __str__(self):
        return self.series

 #  def get_absolute_url(self):   #функция, указывающая путь куда положить со ввода данных с сайта
 #       return reverse_lazy("serie-list-view")
#        return reverse_lazy("serie-detail")   #возвращает в карточку DetailView


class Format(models.Model):
    # pk
    format = models.CharField(
        "Формат",
        max_length=20)
    
    class Meta:
        db_table = 'Формат'
        verbose_name_plural = "Форматы"

    def __str__(self):
        return self.format

class binding(models.Model):
    # pk
    binding = models.CharField(
        "Переплет",
        max_length=20, default=" ")
    
    def get_absolute_url(self):
        return reverse_lazy("binding-list-view")

    class Meta:
        db_table = 'Переплет'
        verbose_name_plural = "Переплеты"

    def __str__(self):
        return self.binding

class Genres(models.Model):
    genres = models.CharField("Жанр",
    max_length=30)

    class Meta:
        db_table = 'Жанр'
        verbose_name_plural = "Жанры"

    def __str__(self):
        return self.genres

class Publisher(models.Model):
    publisher = models.CharField("Издательсьво",
                            max_length=30)

    class Meta:
        db_table = 'Издательство'
        verbose_name_plural = "Издательства"

    def __str__(self):
        return self.publisher
