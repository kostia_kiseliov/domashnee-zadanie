from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.base import TemplateView
from django.views.generic.edit import DeleteView
from catalog.models import Authors, Serie, Format, binding, Genres, Publisher
from catalog.form import SearchForm
from book.models import Book
from django.db.models import Q
from catalog.form import SerieCreateViews, SerieUpdateViews, FormatCreateViews, bindingCreateViews, GenresCreateViews, PublisherCreateViews, AuthorsCreateViews, BookCreateViews
from django.urls import reverse_lazy



class SerieCreateView(CreateView):
   model=Serie
   form_class = SerieCreateViews
   def get_success_url(self, **kwargs):
      return reverse_lazy("serie-detail", kwargs={"pk": self.object.pk})
#   success_url = None привязать к урлу


class SerieUpdateView(UpdateView):
   model = Serie
   fields = ["series"]
   template_name_suffix = '_update_form'
   success_url = reverse_lazy("serie-list-view")

   
   
class SerieDeleteView(DeleteView):
   model = Serie
   success_url = reverse_lazy("serie-list-view")
   

class AuthorsCreateView(CreateView):
   model = Authors
   form_class = AuthorsCreateViews
   template_name_suffix="_create_form"
   def get_success_url(self, **kwargs):
      rederect = self.request.POST.get("detail")
      if rederect:
         reverse_lazy("authors-detail")
      return reverse_lazy("authors-list-view")

class AuthorsUpdateView(UpdateView):
   model=Authors
   fields=["last_name",
           "first_name",
            "middle_name",
         ]
   template_name_suffix="_update_form"
   success_url=reverse_lazy("authors-list-view")

class AuthorsDeleteView(DeleteView):
   model=Authors
   success_url=reverse_lazy("authors-list-view")
   
class FormatCreateView(CreateView):
   model = Format
   form_class = FormatCreateViews
   template_name_suffix = "_create"

   def get_success_url(self, **kwargs):
      rederect = self.request.POST.get("detail")
      if rederect:
         reverse_lazy("format-detail")
      return reverse_lazy("format-list-view")


class FormatUpdateView(UpdateView):
   model = Format
   fields = ["format"]
   success_url = reverse_lazy("format-list-view")


class FormatDeleteView(DeleteView):
   model = Format
   success_url = reverse_lazy("format-list-view")

class bindingCreateView(CreateView):
   model = binding
   form_class = bindingCreateViews

   def get_success_url(self, **kwargs):
      rederect = self.request.POST.get("detail")
      if rederect:
         reverse_lazy("bilding-detail")
      return reverse_lazy("binding-list-view")

class GenresCreateView(CreateView):
   model = Genres
   form_class = GenresCreateViews

   def get_success_url(self, **kwargs):
      rederect = self.request.POST.get("detail")
      if rederect:
         reverse_lazy("genres-detail")
      return reverse_lazy("genres-list-view")

class PublisherCreateView(CreateView):
   model = Publisher
   form_class = PublisherCreateViews
   def get_success_url(self, **kwargs):
         rederect = self.request.POST.get("detail")
         if rederect:
            reverse_lazy("publisher-detail")
         return reverse_lazy("publisher-list-view")

class BookCreateView(CreateView):
   model = Book
   form_class = BookCreateViews
   template_name_suffix="_create"
   def get_success_url(self, **kwargs):
      return reverse_lazy("book-detail", kwargs={"pk": self.object.pk})

class AuthorsDetail(DetailView):
   model = Authors

class bindingDetail(DetailView):
   model = binding

class FormatDetail(DetailView):
   model = Format

class GenresDetail(DetailView):
   model = Genres

class PublisherDetail(DetailView):
   model = Publisher

class SerieDetail(DetailView):
   model = Serie

class BookDetail(DetailView):
   model = Book

class AuthorsList(ListView):
   model = Authors

   def get_queryset(self):
      qs = super().get_queryset()
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n == 0:
         return qs
      return qs.filter(last_name=n, first_name=n, middle_name=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context


class SerieList(ListView):
   model = Serie

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data = self.request.GET  # весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      if n == 0:
         return qs
      return qs.filter(series=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context

class FormatList(ListView):
   model = Format

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n==0:
         return qs
      return qs.filter(format=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context

class bindingList(ListView):
   model = binding

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n==0:
         return qs
      return qs.filter(binding=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context

class GenresList(ListView):
   model = Genres

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n==0:
         return qs
      return qs.filter(genres=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context

class PublisherList(ListView):
   model = Publisher

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n==0:
         return qs
      return qs.filter(publisher=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context

class AllReferenses(TemplateView):
   template_name = "catalog/AllReferenses.html"

class BookList(ListView):
   model = Book

   def get_queryset(self, **kwargs):
      qs = super().get_queryset(**kwargs)
      data=self.request.GET #весь словарь выделяем
      n = data.get("search", 0)  # задаем переменную в строке поиска
      # указываем на конкретную переменную из класса
      if n==0:
         return qs
      return qs.filter(name=n)

   def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      f=SearchForm()
      context["form"]=f
      return context


class BookUpdateView(UpdateView):
   model = Book
   fields = ["name", "ISBN", "age_limit", "price", "serie",
             "last_name", "format_b", "available", "rate",
             "page", "book_binding", "weight"]
   success_url = reverse_lazy("book-detail")
 
class bindingUpdateView(UpdateView):
   model=binding
   fields=["binding"]
   template_name_suffix="_update_form"

class bindingDeleteView(DeleteView):
   model=binding
   success_url = reverse_lazy('binding-list-view')

class BookDeleteView(DeleteView):
   model=Book
   success_url=reverse_lazy("book-list")

class GenresUpdate(UpdateView):
   model=Genres
   fields = ["genres"]
   template_name_suffix="_update"
   success_url = reverse_lazy("genres-list-view")

class GenresDelete(DeleteView):
   model=Genres
   success_url = reverse_lazy("genres-list-view")



def index(request):
    context = locals()
    return render(request, 'D:/проект/mysite/templates/test.html', context)


