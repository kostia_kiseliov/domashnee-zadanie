from django import forms
from django.forms import ModelForm
from .models import Serie, Format, binding, Genres, Publisher, Authors
from book.models import Book


class SearchForm(forms.Form):
    search=forms.CharField()
    active=forms.BooleanField(label="Активный", required=False)


class SerieCreateViews(ModelForm):
    class Meta:
        model=Serie
        fields=(
            'series',
        )


class SerieUpdateViews(ModelForm):
    class Meta:
        model = Serie
        fields = (
            'series',
        )


class FormatCreateViews(ModelForm):
    class Meta:
        model = Format
        fields = (
            'format',
        )


class bindingCreateViews(ModelForm):
    class Meta:
        model = binding
        fields = (
            'binding',
        )


class GenresCreateViews(ModelForm):
    class Meta:
        model = Genres
        fields = (
            'genres',
        )


class PublisherCreateViews(ModelForm):
    class Meta:
        model = Publisher
        fields = (
            'publisher',
        )


class AuthorsCreateViews(ModelForm):
    class Meta:
        model = Authors
        fields = (
            'first_name',
            'last_name',
            'middle_name',
        )


class BookCreateViews(ModelForm):
    class Meta:
        model = Book
        fields = (
            'name',
            'ISBN',
            'age_limit',
            'image',
            'price',
            'serie',
            'last_name',
            'format_b',
            'issue_year',
            'available',
            'rate',
            'page',
            'book_binding',
            'weight',
            
        )

class BookUpdateViews(ModelForm):
    class Meta:
        model=Book
        fields = (
            'name',
            'ISBN',
            'age_limit',
            'image',
            'price',
            'serie',
            'last_name',
            'format_b',
            'issue_year',
            'available',
            'rate',
            'page',
            'book_binding',
            'weight',

        )

#class AddBookForm(form.ModelForm):
#    model=BookInCart

#    fields=[
#        "book",
#        "quant"
#    ]      
# labеl="Поиск"
