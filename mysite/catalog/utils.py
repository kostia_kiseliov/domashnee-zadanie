from book.models import *
from catalog.models import Authors, Serie, Format, binding, Genres, Publisher


def create_authors(first_n, last_n, middle_n, obj):
    obj = Authors(first_name=first_n, last_name=last_n,
                      middle_name=middle_n)
    obj.save()
    print("создали нового автора {}, с ключем={}".format(obj.last_name, obj.pk))


def create_serie(series_s):
    obj = Serie(series=series_s)
    obj.save()
    print("создали серию {}, с ключем={}".format(obj.series, obj.pk))


def create_format(name_f, format_book):
    obj = Format(name=name_f, format=format_book)
    obj.save()
    print("создан новый формат {}, с ключем={}".format(obj.format, obj.pk))


def create_binding(name_bin, binding_b):
    obj = binding(name=name_bin, binding=binding_b)
    obj.save()
    print("создан новый переплет {}, с ключем={}".format(obj.binding, obj.pk))


def create_genres(name_g, genres_b):
    obj = Genres(name=name_g, genres=genres_b)
    obj.save()
    print("создан новый жанр {}, с ключем={}".format(obj.genres, obj.pk))


def create_publisher(name_p, publisher_b):
    obj = Publisher(name=name_p, publisher=publisher_b)
    obj.save()
    print("создан новое издательство {}, с ключем={}".format(obj.publisher, obj.pk))


def delete_object(name, primary_key):
    name.objects.get(pk=primary_key).delete()
    print("из справочника был удален объект {}".format(obj.name))


def create_list(x):
    a_list = []
    for i in range(1, (x+1)):
        obj = Serie(series="Serie {}".format(i))
        a_list.append(obj)
    Serie.objects.bulk_create(a_list)


def count_by_name(part_name):
    x = Authors.objects.filter(last_name=part_name).count()
    print('Количество авторов, которые содержат "{}" - {}'.format(part_name, x))


def update_create(authors_b):
    obj, created = Authors.objects.update_or_create(
        authors_b=last_name)


def list_of(name, pr_key):
    obj = name.objects.get(pk=pr_key)
    for i in obj.Books.all():
        print(i)


def create_book(bk):
    obj = Book(name=bk['name'], price=bk['price'], issue_year=bk['issue_year'], page=bk['page'], ISBN=bk['ISBN'], weight=bk['weight'],
               date_of_change=bk['date_of_change'])
    aut = Authors.objects.get(pk=bk['last_name'])
    obj.series = Serie.objects.get(name=bk['series'])
    obj.format = Format.objects.get(pk=bk['format_b'])
    obj.binding_b = binding.objects.get(binding=bk['binding'])

    obj.publish = Publisher.objects.get(name=bk['publisher'])
    obj.save()
